<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kategori".
 *
 * @property string $id_kategori
 * @property string $kategori_barang
 *
 * @property Barang[] $barangs
 */
class Kategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kategori', 'kategori_barang'], 'required'],
            [['id_kategori'], 'string', 'max' => 7],
            [['kategori_barang'], 'string', 'max' => 10],
            [['id_kategori'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'kategori_barang' => 'Kategori Barang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarangs()
    {
        return $this->hasMany(Barang::className(), ['kategori_barang' => 'id_kategori']);
    }
}
