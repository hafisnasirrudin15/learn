<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "barang".
 *
 * @property string $id_barang
 * @property string $nama_barang
 * @property int $harga_barang
 * @property string $kategori_barang
 *
 * @property Kategori $kategoriBarang
 */
class Barang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'nama_barang', 'harga_barang', 'kategori_barang'], 'required'],
            [['harga_barang'], 'integer'],
            [['id_barang'], 'string', 'max' => 7],
            [['nama_barang'], 'string', 'max' => 20],
            [['kategori_barang'], 'string', 'max' => 10],
            [['id_barang'], 'unique'],
            [['kategori_barang'], 'exist', 'skipOnError' => true, 'targetClass' => Kategori::className(), 'targetAttribute' => ['kategori_barang' => 'id_kategori']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_barang' => 'Id Barang',
            'nama_barang' => 'Nama Barang',
            'harga_barang' => 'Harga Barang',
            'kategori_barang' => 'Kategori Barang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKategoriBarang()
    {
        return $this->hasOne(Kategori::className(), ['id_kategori' => 'kategori_barang']);
    }
}
